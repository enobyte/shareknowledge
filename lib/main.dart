import 'package:flutter/material.dart';

import 'Login.dart';
import 'main_menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Login(),
      routes: <String, WidgetBuilder>{
        '/main_menu': (BuildContext context) => new MainMenu(),
      },
    );

  }
}

//class MyHomePage extends StatefulWidget {
//  MyHomePage({Key key, this.title}) : super(key: key);
//  final String title;
//
//  @override
//  _MyHomePageState createState() => _MyHomePageState();
//}
//
//class _MyHomePageState extends State<MyHomePage> {
//  final utils = Utils();
//  final usernameController = TextEditingController();
//  final passwordController = TextEditingController();
//
//  _incrementCounter() {
//    setState(() {});
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        // Here we take the value from the MyHomePage object that was created by
//        // the App.build method, and use it to set our appbar title.
//        title: Text(widget.title),
//      ),
//      body: Center(
//        child: Column(
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            Text(
//              'username : ' + usernameController.text,
//            ),
//            Text('password : ${passwordController.text}'),
//            TextField(
//              controller: usernameController,
//              decoration: InputDecoration(hintText: "Username"),
//            ),
//            TextField(
//              controller: passwordController,
//              decoration: InputDecoration(hintText: "Password"),
//            )
//          ],
//        ),
//      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: () => _incrementCounter(),
//        tooltip: 'Increment',
//        child: Icon(Icons.add),
//      ), // This trailing comma makes auto-formatting nicer for build methods.
//    );
//  }
//}
