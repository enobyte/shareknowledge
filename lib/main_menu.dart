import 'package:flutter/material.dart';

import 'detail_main.dart';

class MainMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainMenuState();
  }
}

class MainMenuState extends State<MainMenu> {
  final txtController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextField(
            controller: txtController,
          ),
          Container(
            color: Colors.white,
            child: RaisedButton(
              child: Text('CLICK'),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailMain(txtController.text)),
              ),
            ),

          ),
        ],
      )
    );
  }
}
