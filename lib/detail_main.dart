import 'package:flutter/material.dart';

class DetailMain extends StatelessWidget {
  String data = "";

  DetailMain(this.data);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Menu'),
      ),
      body: Container(
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 30),
              child: Text(this.data),
            ),
            Text(this.data),
          ],
        ),
      ),
    );
  }
}
