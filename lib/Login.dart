import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

//Navigator.pushNamedAndRemoveUntil(context, "/main_menu", (_) => false);
//Navigator.pop()

class LoginState extends State<Login> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  //_scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(
            'Login Form',
            style: TextStyle(color: Colors.yellow),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Positioned.fill(
                child: Image.asset(
              'lib/images/img.jpg',
              fit: BoxFit.fill,
            )),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 50, right: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'lib/images/img.jpg',
                      width: 250,
                      height: 250,
                    ),
                    Image.network(
                      'https://banner2.kisspng.com/20180404/ojq/kisspng-cut-flowers-floral-design-bunga-5ac567df9cf2d0.6734140315228866236429.jpg',
                    ),
                    TextField(
                      controller: _emailController,
                      decoration: InputDecoration(hintText: 'Email'),
                    ),
                    TextField(
                      obscureText: true,
                      controller: _passwordController,
                      decoration: InputDecoration(hintText: 'Password'),
                    ),
                    RaisedButton(
                      onPressed: () => Navigator.pushNamedAndRemoveUntil(context, "/main_menu", (_) => false),
                      color: Colors.yellow,
                      child: Text('Login'),
                    )
                  ],
                ),
              ),
            )
          ],
        ));
  }

  void showSnacbar(String email, String pass) {
    _scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text(email + ' - ' + pass)));
  }
}
